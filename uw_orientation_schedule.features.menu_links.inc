<?php

/**
 * @file
 * uw_orientation_schedule.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_orientation_schedule_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-management_orientation-schedule-settings:admin/config/system/orientation-schedule-settings.
  $menu_links['menu-site-management_orientation-schedule-settings:admin/config/system/orientation-schedule-settings'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/orientation-schedule-settings',
    'router_path' => 'admin/config/system/orientation-schedule-settings',
    'link_title' => 'Orientation schedule settings',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_orientation-schedule-settings:admin/config/system/orientation-schedule-settings',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Orientation schedule settings');

  return $menu_links;
}
