<?php

/**
 * @file
 * uw_orientation_schedule.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_orientation_schedule_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'orientation schedule settings'.
  $permissions['orientation schedule settings'] = array(
    'name' => 'orientation schedule settings',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'uw_orientation_schedule',
  );

  return $permissions;
}
