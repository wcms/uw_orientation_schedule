<?php

/**
 * @file
 * Call the utility function in uw_orientation_schedule to get the dates for this template.
 */
$dates = _uw_orientation_schedule_dates('uw_orientation_schedule_graduate_start_date');
?>
<div class="uw_orientation_schedule_wrapper">
  <iframe src="https://www.google.com/calendar/embed?src=ne6du3vp0akhc2uq4nglbs3irs%40group.calendar.google.com&ctz=America/Toronto&mode=AGENDA&amp;dates=<?php print $dates; ?>" style="border: 0" width="500" height="375" frameborder="0" scrolling="no"></iframe>
</div>
