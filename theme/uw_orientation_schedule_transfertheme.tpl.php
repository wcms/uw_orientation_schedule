<?php

/**
 * @file
 * Call the utility function in uw_orientation_schedule to get the dates for this template.
 */
$dates = _uw_orientation_schedule_dates('uw_orientation_schedule_transfer_start_date');
?><div class="uw_orientation_schedule_wrapper">
  <form name="input" action="" method="post">
  <p>
  <label for="academic">Select your faculty/program:</label>
  <select id="academic" name="academic">
    <option value=""></option>  
    <?php foreach ($faculty_programs as $google_code => $values): ?>
    <option value='<?php print $google_code ?>' <?php print $values[1] ?>><?php print $values[0] ?></option>
    <?php endforeach; ?>
  </select>
  </p>
  <p>
  <input type="submit" value="Submit" name="submit" />
  </p>
  </form>  
  <br/><hr/><br/>

  <?php if (($is_post == TRUE) && ($is_valid_post == TRUE)): ?>
  <iframe src="https://www.google.com/calendar/embed?title=<?php print $faculty_programs[$academic][0] ?> Schedule&amp;showNav=0&amp;showCalendars=0&amp;mode=AGENDA&amp;height=800&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=<?php print $academic ?>%40group.calendar.google.com&amp;color=%23<?php print $faculty_programs[$academic][2] ?>&amp;ctz=America%2FToronto&amp;dates=<?php print $dates; ?>" style=" border-width:0 " width="100%" height="800" frameborder="0" scrolling="no"></iframe>
  <?php elseif (($is_post == TRUE) && ($is_valid_post == FALSE)): ?>
  <p><em>Please select a faculty/program.</em></p>
  <?php endif; ?>
</div>
