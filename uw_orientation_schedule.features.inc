<?php

/**
 * @file
 * uw_orientation_schedule.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_orientation_schedule_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
